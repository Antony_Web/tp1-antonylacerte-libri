class Formations {
    /**
     * Nom Formation
     * Description
     * lien
     */
    constructor(nom, description, lien){
        this.nom = nom
        this.description = description
        this.lien = lien
    }
}

/* Informations à distribuer */
const formations = [
    new Formations("Formation numéro 1", "Description:", "Lien:"),
    new Formations("Formation numéro 2", "Description:", "Lien:"),
    new Formations("Formation numéro 3", "Description:", "Lien:"),
    new Formations("Formation numéro 4", "Description:", "Lien:"),
    new Formations("Formation numéro 5", "Description:", "Lien:")
]

exports.allFormations = formations