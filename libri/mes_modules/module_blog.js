class Articles {
    /**
     * Nom Article
     * courte description
     * lien
     */
    constructor(nom, description, lien){
        this.nom = nom
        this.description = description
        this.lien = lien
    }
}

/* Informations à distribuer */
const articles = [
    new Articles("Article numéro 1", "Courte description une:", "Cliquez ici pour voir l'article"),
    new Articles("Article numéro 2", "Courte description deux:", "Cliquez ici pour voir l'article"),
    new Articles("Article numéro 3", "Courte description trois:", "Cliquez ici pour voir l'article"),
    new Articles("Article numéro 4", "Courte description quatre:", "Cliquez ici pour voir l'article"),
    new Articles("Article numéro 5", "Courte description cinq:", "Cliquez ici pour voir l'article"),
    new Articles("Article numéro 6", "Courte description six:", "Cliquez ici pour voir l'article"),
    new Articles("Article numéro 7", "Courte description sept:", "Cliquez ici pour voir l'article"),
    new Articles("Article numéro 8", "Courte description huit:", "Cliquez ici pour voir l'article"),
    new Articles("Article numéro 9", "Courte description neuf:", "Cliquez ici pour voir l'article"),
    new Articles("Article numéro 10", "Courte description dix:", "Cliquez ici pour voir l'article"),
]

exports.allArticles = articles