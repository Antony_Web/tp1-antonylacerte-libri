var express = require('express');
const { allArticles } = require('../mes_modules/module_blog');
var router = express.Router();
var formations = require('../mes_modules/module_formations')
var blog = require("../mes_modules/module_blog")

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title:'Accueil'});
});

/* GET formations */
router.get('/formations', function(req, res, next){
  res.render("nos_formations", { title: "Nos formations", formations: formations.allFormations })
})

/* GET Page_formation */
router.get('/page_formation', function(req, res, next){
  res.render("page_formation", { title: "Page Formation"})
})

/* GET blog */
router.get("/blog", function(req, res, next){
  res.render("blog", { title: "Notre Blog", blog: blog.allArticles})
})
/* GET Page_article */
router.get("/page_article", function(req, res, next){
  res.render("page_article", { title: "Page Article"})
})

/* GET contact */
router.get("/contact", function(req, res, next){
  res.render("contact", { title: "Nous contacter"})
})

module.exports = router;